import axios from 'axios';
import { useEffect, useState } from 'react';

function App() {

  const [state, setState] = useState({loading: false, value: {}});

  const loadValue = (() => {
      setState({loading: true, value: {}})
      axios.get('https://dummyjson.com/products/1').then((res) => {
        setState({loading: false, value: res.data})
      }).catch((err) => {
        setState({loading: false, value: err})
      })
  })

  return (
    <div className="App">
      <button onClick={loadValue}>{state.loading? <>Loading..</> :<>Fetch Value</>} </button>
      <pre>{JSON.stringify(state.value, null, 2)}</pre>     
    </div>
  );
}

export default App;
